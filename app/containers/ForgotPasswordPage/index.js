/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Breadcrumb } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import Forgot from '../../components/Authentification/Forgot';

const sections = [
  { key: 'home', content: 'Home', to: '/', as: Link },
  { key: 'forgot', content: 'Forgot Password', active: true },
];

/* eslint-disable react/prefer-stateless-function */
export default class ForgotPasswordPage extends React.PureComponent {
  render() {
    return (
      <div>
        <Breadcrumb
          style={{ padding: '2em' }}
          divider="/"
          sections={sections}
        />
        <Forgot />
      </div>
    );
  }
}
