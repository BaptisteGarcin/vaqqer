/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  en: {
    id: 'boilerplate.containers.LocaleToggle.en',
    defaultMessage: 'en',
  },
  fr: {
    id: 'boilerplate.containers.LocaleToggle.fr',
    defaultMessage: 'fr',
  },
  ar: {
    id: 'boilerplate.containers.LocaleToggle.ar',
    defaultMessage: 'ar',
  },
});
