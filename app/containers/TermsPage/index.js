/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Breadcrumb, Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

const sections = [
  { key: 'home', content: 'Home', to: '/', as: Link },
  { key: 'terms', content: 'Terms', active: true },
];

/* eslint-disable react/prefer-stateless-function */
export default class TermsPage extends React.PureComponent {
  render() {
    return (
      <div>
        <Breadcrumb
          style={{ padding: '2em' }}
          divider="/"
          sections={sections}
        />
        <h1>
          <Header
            textAlign="center"
            size="huge"
            style={{ marginBottom: '27rem', marginTop: '10rem' }}
          >
            <FormattedMessage id="terms" defaultMessage="Terms page" />
          </Header>
        </h1>
      </div>
    );
  }
}
