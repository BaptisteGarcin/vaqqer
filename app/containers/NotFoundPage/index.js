/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Header } from 'semantic-ui-react';

/* eslint-disable react/prefer-stateless-function */
export default class NotFound extends React.PureComponent {
  render() {
    return (
      <h1>
        <Header
          textAlign="center"
          size="huge"
          style={{ marginBottom: '29rem', marginTop: '10rem' }}
        >
          <FormattedMessage
            id="404"
            defaultMessage="Error 404 Page Not Found"
          />
        </Header>
      </h1>
    );
  }
}
