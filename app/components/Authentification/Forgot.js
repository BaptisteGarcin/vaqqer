import React, { Component } from 'react';
import {
  Form,
  Grid,
  Header,
  Segment,
  Divider,
  Message,
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { injectIntl, FormattedMessage } from 'react-intl';

class Forgot extends Component {
  state = {
    email: '',
  };

  handleSubmit = () => {
    axios
      .post('/forgot', {
        email: this.state.email,
      })
      .then(response => {})
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const { email } = this.state;
    const { intl } = this.props;
    return (
      <div>
        <Grid
          textAlign="center"
          style={{ height: '100%', margin: '1rem' }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header
              as="h2"
              color="teal"
              textAlign="center"
              style={{ marginTop: '3rem', marginBottom: '3rem' }}
            >
              <FormattedMessage id="app.components.Forgot.description" />
            </Header>
            <Form size="large" onSubmit={this.handleSubmit}>
              <Segment raised>
                <Form.Input
                  name="email"
                  type="email"
                  autoFocus="autofocus"
                  autoComplete="email"
                  required
                  value={email}
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder={intl.formatMessage({
                    id: 'Email',
                  })}
                  onChange={e => this.setState({ email: e.target.value })}
                />
                <Form.Button color="teal" fluid size="large">
                  <FormattedMessage id="app.components.Forgot.send" />
                </Form.Button>
              </Segment>
            </Form>
            <Divider horizontal>
              <FormattedMessage id="Or" />
            </Divider>
            <Message style={{ marginBottom: '3rem' }}>
              <FormattedMessage id="NewToUs" />
              <Link to="/register">
                <FormattedMessage id="SignUp" />
              </Link>
              <br />
              <Divider />
              <Link to="/logging">
                <FormattedMessage id="AlreadyHaveAnAccount" />
              </Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default injectIntl(Forgot);
