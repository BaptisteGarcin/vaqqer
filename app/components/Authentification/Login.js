import React, { Component } from 'react';
import {
  Form,
  Grid,
  Header,
  Message,
  Segment,
  Divider,
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';

class Login extends Component {
  state = {
    email: '',
    password: '',
  };

  handleSubmit = () => {
    axios
      .post('/login', {
        email: this.state.email,
        password: this.state.password,
      })
      .then(response => {
        this.props.dispatch(push(response.data.redirect));
        if (response.data.redirect) {
          window.location.reload();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const { password, email } = this.state;
    const { intl } = this.props;
    return (
      <Grid
        textAlign="center"
        style={{ height: '100%', margin: '1rem' }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header
            as="h2"
            color="teal"
            textAlign="center"
            style={{ marginTop: '3rem', marginBottom: '3rem' }}
          >
            <FormattedMessage id="app.components.Login.login" />
          </Header>
          <Form size="large" onSubmit={this.handleSubmit}>
            <Segment raised>
              <Form.Input
                name="email"
                type="email"
                autoFocus="autofocus"
                autoComplete="email"
                required
                value={email}
                fluid
                icon="user"
                iconPosition="left"
                placeholder={intl.formatMessage({
                  id: 'Email',
                })}
                onChange={e => this.setState({ email: e.target.value })}
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder={intl.formatMessage({
                  id: 'Password',
                })}
                name="password"
                type="password"
                required
                value={password}
                onChange={e => this.setState({ password: e.target.value })}
              />
              <Form.Button color="teal" fluid size="large">
                <FormattedMessage id="Login" />
              </Form.Button>
            </Segment>
          </Form>
          <Divider horizontal>Or</Divider>
          <Message style={{ marginBottom: '3rem' }}>
            <FormattedMessage id="NewToUs" />
            <Link to="/register">
              <FormattedMessage id="SignUp" />
            </Link>
            <br />
            <Divider />
            <Link to="/forgotPass">
              <FormattedMessage id="ForgotPassword" />
            </Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(connect(mapDispatchToProps)(Login));
