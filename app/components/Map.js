import React, { PureComponent } from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { FormattedMessage } from 'react-intl';

export default class MapLeaflet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 51.505,
      lng: -0.09,
      zoom: 13,
      addMarker: this.props.addMarker,
    };
  }

  componentDidMount = () => {
    console.log('mounted', this.props.latlng);
    if (this.props.latlng) {
      this.setState({ lat: this.props.latlng[0], lng: this.props.latlng[1] });
    }
  };

  componentWillReceiveProps = () => {
    console.log('new props');
    if (this.props.latlng) {
      this.setState({ lat: this.props.latlng[0], lng: this.props.latlng[1] });
    }
  };

  render() {
    const position = [this.state.lat, this.state.lng];
    return (
      <Map
        center={position}
        zoom={this.state.zoom}
        onClick={e => {
          console.log(e);
          if (this.state.addMarker) {
            this.setState({ lat: e.latlng.lat, lng: e.latlng.lng });
          }
        }}
      >
        <TileLayer
          attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
        />
        <Marker position={position}>
          <Popup>
            <FormattedMessage id="app.components.YourLocation" />
          </Popup>
        </Marker>
      </Map>
    );
  }
}
