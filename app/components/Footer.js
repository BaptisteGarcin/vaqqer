import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Header, Divider } from 'semantic-ui-react';
class Footer extends Component {
  state = {};

  render() {
    return (
      <div>
        <Divider fitted />
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '3em',
          }}
        >
          <FormattedMessage id="app.components.Footer.content" />
          <Header color="black" floated="right">
            Vaqqer 2018
          </Header>
        </div>
      </div>
    );
  }
}

export default Footer;
