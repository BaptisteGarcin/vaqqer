/* eslint-disable global-require */
const passport = require('passport');

/**
 * Front-end middleware
 */
module.exports = (app, options) => {
  /**
   * Controllers (route handlers).
   */
  const userController = require('../controllers/user');

  /**
   * API keys and Passport configuration.
   */
  const passportConfig = require('../config/passport');

  /**
   * Primary app routes.
   */
  app.post('/login', userController.postLogin);
  app.get('/logout', userController.logout);
  app.post('/forgot', userController.postForgot);
  app.get('/reset/:token', userController.getReset);
  app.post('/reset/:token', userController.postReset);
  app.post('/signup', userController.postSignup);
  app.post(
    '/account/profile',
    passportConfig.isAuthenticated,
    userController.postUpdateProfile,
  );
  app.post(
    '/account/password',
    passportConfig.isAuthenticated,
    userController.postUpdatePassword,
  );
  app.post(
    '/account/delete',
    passportConfig.isAuthenticated,
    userController.postDeleteAccount,
  );
  app.get(
    '/account/unlink/:provider',
    passportConfig.isAuthenticated,
    userController.getOauthUnlink,
  );

  app.get('/api/users', userController.getAllUsers);
  app.put('/api/user', userController.updateAnUser);
  app.get('/api/user', userController.getAnUser);
  app.put('/api/user/favorites', userController.pushFavorite);
  app.get('/api/user/favorites', userController.getFavorites);
  app.put('/api/user/distance', userController.updateDistance);

  const isProd = process.env.NODE_ENV === 'production';

  if (isProd) {
    const addProdMiddlewares = require('./addProdMiddlewares');
    addProdMiddlewares(app, options);
  } else {
    const webpackConfig = require('../../internals/webpack/webpack.dev.babel');
    const addDevMiddlewares = require('./addDevMiddlewares');
    addDevMiddlewares(app, webpackConfig);
  }

  return app;
};
